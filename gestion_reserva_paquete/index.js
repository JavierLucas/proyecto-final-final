const express = require('express');
const logger = require('morgan');

const app = express();
const axios = require('axios');
const config = require('./config');
const {port} = require('./config');
const {auth} = require("./services/auth");

process.env.NODE_TLS_REJECT_UNAUTHORIZED = 0;

//Middlware para que entienda formato formulario
app.use(express.urlencoded({extended:false}));
//Middleware para que entienda json
app.use(express.json());


//Declaramos nuestro servicio de https
const https = require('https');

//Cargamos los certificados, necesitamos acceder a los archivos
const fs = require('fs');
const { ok } = require('assert');
const HTTPS_OPTIONS = {
    key: fs.readFileSync('./cert/key.pem'),
    cert: fs.readFileSync('./cert/cert.pem')
};

var accessLogStream = fs.createWriteStream('./access.log', {flags:'a'});

app.use(logger('combined', {stream:{
    write: function(str){
        accessLogStream.write(str);
        console.log(str);
    }
}}));


app.post('/api/v0/paquetes', auth,(req,res,next) =>{
    var coches = req.body.coches;
    var vuelos = req.body.vuelos;
    var hoteles = req.body.hoteles;
    var pagos = req.body.pagos;

    var totalResponse = {};

    async function realizaLlamadas(){
        var importe = 0;
        if(coches){
            coches.userID = req.usuario._id;
            try{
                //Reservo coches (gestion reserva coche post)
                const response = await axios.post(`${config.coches}/cochesT`, coches, {
                    headers: {Authorization: `Bearer ${req.usuario.token}`}
                });
                totalResponse.coches = response.data.coches;
                importe += response.data.coches.precio;

            }
            catch(err){

                if(err.response){
                    res.status(err.response.status).json(err.response.data);
                }
                else{
                    res.status(500).json({
                        resultado: "Se ha producido un error",
                        error: err.message
                    })
                }
                throw new Error('Fin de transacción')

            }
        }
        if(vuelos){
             
            vuelos.userID = req.usuario._id;
            try{
                const response = await axios.post(`${config.vuelos}/vuelosT`, vuelos, {
                    headers: {Authorization: `Bearer ${req.usuario.token}`}
                });
                totalResponse.vuelos = response.data.vuelos;
                importe += response.data.vuelos.precio;

            }
            catch(err){
                if (coches){

                    //Compensamos el coche (gestion coche, delete)
                    const response = await axios.delete(`${config.coches}/cochesT/${coches._id}`,  {
                        headers: {Authorization: `Bearer ${req.usuario.token}`}

                    })                  

                }
                if(err.response){
                    res.status(err.response.status).json(err.response.data);
                }
                else{
                    res.status(500).json({
                        resultado: "Se ha producido un error",
                        error: err.message
                    })
                }
                throw new Error('Fin de transacción')

            }
        }
        if(hoteles){
            hoteles.userID = req.usuario._id;
            try{
                const response = await axios.post(`${config.hoteles}/hotelesT`, hoteles, {
                    headers: {Authorization: `Bearer ${req.usuario.token}`}
                });
                totalResponse.hoteles = response.data.hoteles;
                importe += response.data.hoteles.precio;

            }
            catch(err){

                if (coches){
                    //Deshacemos coches
                    const response = await axios.delete(`${config.coches}/cochesT/${coches._id}`,  {
                        headers: {Authorization: `Bearer ${req.usuario.token}`}

                    })
                }
                if(vuelos){
                    //deshacemos vuelos
                    const response = await axios.delete(`${config.vuelos}/vuelosT/${vuelos._id}`,  {
                        headers: {Authorization: `Bearer ${req.usuario.token}`}

                    })
                }
                if(err.response){
                    res.status(err.response.status).json(err.response.data);
                }
                else{
                    res.status(500).json({
                        resultado: "Se ha producido un error",
                        error: err.message
                    })
                }
                throw new Error('Fin de transacción')


            }

        }
        //Me falta gestionar el pago (voy acumulando el total y después lo sumo todo)
        //Realizamos la llamada al banco
        console.log(`IMPORTE ${importe}`);
        pagos.importe = importe;
        try{
            await axios.post(`${config.pagos}/pagos`, pagos);
        }
        catch(err){

            if (coches){
                //Deshacemos coches
                const response = await axios.delete(`${config.coches}/cochesT/${coches._id}`,  {
                    headers: {Authorization: `Bearer ${req.usuario.token}`}

                })
            }
            if(vuelos){
                //deshacemos vuelos
                const response = await axios.delete(`${config.vuelos}/vuelosT/${vuelos._id}`,  {
                    headers: {Authorization: `Bearer ${req.usuario.token}`}

                })
            }
            if(hoteles){
                //deshacemos hoteles
                const response = await axios.delete(`${config.hoteles}/hotelesT/${hoteles._id}`,  {
                    headers: {Authorization: `Bearer ${req.usuario.token}`}

                })
            }
            if(err.response){
                res.status(err.response.status).json(err.response.data);
            }
            else{
                res.status(500).json({
                    resultado: "Se ha producido un error",
                    error: err.message
                })
            }
            throw new Error('Fin de transacción')

        }
        res.json(totalResponse)

    }
    
    
    realizaLlamadas()


        
})



//Lanzamos el servicio en un modo seguro
https.createServer(HTTPS_OPTIONS, app).listen(port, () => {
    console.log(`Paquete SEGURO ejecutándose en https://localhost:${port}/`);
});