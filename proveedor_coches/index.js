'use strict'
//Coge la variable de puerto PORT si existe, en caso de que no, pásale el puerto
const {port} = require('./config');
const express = require('express');
const logger = require('morgan');
const mongojs = require('mongojs');
const app = express();

const bdURL = require('./config').bdURL

//Declaramos nuestro servicio de https
const https = require('https');

//Cargamos los certificados, necesitamos acceder a los archivos
const fs = require('fs');
const HTTPS_OPTIONS = {
    key: fs.readFileSync('./cert/key.pem'),
    cert: fs.readFileSync('./cert/cert.pem')
};

//Me conecto con la base de datos
//Podría omitir la IP y el puerto
var db = mongojs(bdURL);
//Esto es simplemente un puntero a la función
var id = mongojs.ObjectID;

//Declaramos nuestro middleware
var accessLogStream = fs.createWriteStream('./access.log', {flags:'a'});

app.use(logger('combined', {stream:{
    write: function(str){
        accessLogStream.write(str);
        console.log(str);
    }
}}));
//Middlware para que entienda formato formulario
app.use(express.urlencoded({extended:false}));
//Middleware para que entienda json
app.use(express.json());



app.get('/api/v0/coches/:idReserva', (req,res, next) => {
    const coleccion = 'coches';
    db.collection(coleccion).findOne({ _id: req.params.idReserva}, (error, elemento) => {
        if (error) return next(error);   //Propagamos el error

        //console.log(elemento);
        res.json({
            resultado: 'ok',
            coches: elemento
        });
    });
    
});

app.get('/api/v0/cochesTodos', (req,res,next) => {
    const coleccion = 'coches';
    db.collection(coleccion).find((err, result) =>{
        res.json({
            resultado: 'ok',
            coches: result
        });
    });
});

app.get('/api/v0/coches', (req,res,next) => {
    const coleccion = 'coches';
    db.collection(coleccion).find({reservado:0},(err, result) =>{
        res.json({
            resultado: 'ok',
            coches: result
        });
    });
});



app.post('/api/v0/coches',(req, res, next) =>{
    const nuevoElemento = req.body;
    const coleccion = 'coches';

    db.collection(coleccion).save(nuevoElemento, (err, elementoGuardado) => {
        if (err) return next(err);

        //console.log(elementoGuardado);
        res.status(201).json({
            resultado: 'ok',
            coches: elementoGuardado
        });

    });
});


app.delete('/api/v0/coches/:idReserva', (req, res, next) => {
    const idReserva = req.params.idReserva;
    const coleccion = 'coches';
    //console.log(typeof idReserva);

    db.collection(coleccion).remove(
        //No vamos a utilizar la función id, porque voy a suponer que lo establezo yo
        //{idReserva: id(idReserva)},
        {_id: idReserva},
        (err, resultado) => {
            if (err) return next(error);

            //console.log(resultado);
            res.json({
                resultado: 'Reserva eliminada correctamente',
                //elementoID: id(idReserva),
                reserva: idReserva,
                resumenOperacion: resultado
            });
        });

});


app.put('/api/v0/coches/:idReserva', (req, res, next) => {

    const idReserva = req.params.idReserva;
    const coleccion = 'coches';
    const reservado = req.body.reservado;
    //console.log(typeof idReserva);

    if (reservado === 1){
        db.collection(coleccion).findOne({_id: idReserva}, (err,doc) => {
            if (doc.reservado === 1){
                res.status(400).json({
                    resultado: 'ko',
                    mensaje: 'El servicio solicitado ya no está disponible'
                })
            }
            else{
                db.collection(coleccion).update(
                    {_id: idReserva}, {$set: {reservado: reservado}},
                    (err, resultado) => {
                        if (err) return next(error);
            
                        res.json({
                            resultado: 'Reserva actualizada',
                            //elementoID: id(idReserva),
                            reserva: idReserva,
                            resumenOperacion: resultado
                        });
                    });
            }
        })
    }

    else {
        db.collection(coleccion).update(
            {_id: idReserva}, {$set: {reservado: reservado}},
            (err, resultado) => {
                if (err) return next(error);
    
                //console.log("Estoy actualizando");
                res.json({
                    resultado: 'Reserva actualizada',
                    //elementoID: id(idReserva),
                    reserva: idReserva,
                    resumenOperacion: resultado
                });
            });

    }



});


//Lanzamos el servicio en un modo seguro
https.createServer(HTTPS_OPTIONS, app).listen(port, () => {
    console.log(`Proveedores coche SEGURO ejecutándose en https://localhost:${port}/`);
});