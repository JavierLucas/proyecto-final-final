'use-strict'

const {port} = require('./config');
const express = require('express');
const logger = require('morgan');
const app = express();
const path = require('path')

const mongojs = require('mongojs');
const bdURL = require('./config').bdURL;
var db = mongojs(bdURL);


//Declaramos nuestro servicio de https
const https = require('https');

//Cargamos los certificados, necesitamos acceder a los archivos
const fs = require('fs');
const { ok } = require('assert');
const HTTPS_OPTIONS = {
    key: fs.readFileSync('./cert/key.pem'),
    cert: fs.readFileSync('./cert/cert.pem')
};

var accessLogStream = fs.createWriteStream('./access.log', {flags:'a'});

app.use(logger('combined', {stream:{
    write: function(str){
        accessLogStream.write(str);
        console.log(str);
    }
}}));
//Middlware para que entienda formato formulario
app.use(express.urlencoded({extended:false}));
//Middleware para que entienda json
app.use(express.json());

app.post('/api/v0/pagos', (req,res, next) => {
    const datos = req.body

    db.collection("pagos").findOne({_id: datos._id}, (err, doc) => {
        if (!doc){
            res.status(400).json({
                resultado: 'ko',
                mensaje: 'No existe la cuenta indicada'
            });
        }
        else{
            if(doc.saldo < datos.importe){
                res.status(400).json({
                    resultado: 'ko',
                    mensaje: 'Saldo insuficiente'
                });
            }
            else{
                const nuevoSaldo = doc.saldo - datos.importe;
                console.log(nuevoSaldo);
                db.collection("pagos").update({_id: datos._id}, {$set: {saldo: nuevoSaldo}});
                res.json({
                    resultado: 'ok',
                    mensaje: 'pago realizado correctamente'
                });
            }
        }
    })
})

app.get('/api/v0/pagos', (req,res, next) => {
    db.collection("pagos").find((err, result) => {
        res.json({
            resultado: ok,
            cuentas: result
        })
    })
})



//Lanzamos el servicio en un modo seguro
https.createServer(HTTPS_OPTIONS, app).listen(port, () => {
    console.log(`Pagos SEGURO ejecutándose en https://localhost:${port}/`);
});