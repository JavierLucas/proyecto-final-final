'use strict'
//Coge la variable de puerto PORT si existe, en caso de que no, pásale el puerto
const {port} = require('./config');
const express = require('express');
const logger = require('morgan');
const mongojs = require('mongojs');
const app = express();
const bdURL = require('./config').bdURL;
const {auth} = require('./services/auth');

//Me conecto con la base de datos
//Podría omitir la IP y el puerto
var db = mongojs(bdURL);
//Esto es simplemente un puntero a la función
var id = mongojs.ObjectID;


//Declaramos nuestro servicio de https
const https = require('https');

//Cargamos los certificados, necesitamos acceder a los archivos
const fs = require('fs');
const HTTPS_OPTIONS = {
    key: fs.readFileSync('./cert/key.pem'),
    cert: fs.readFileSync('./cert/cert.pem')
};

//Declaramos nuestro middleware
var accessLogStream = fs.createWriteStream('./access.log', {flags:'a'});

app.use(logger('combined', {stream:{
    write: function(str){
        accessLogStream.write(str);
        console.log(str);
    }
}}));
//Middlware para que entienda formato formulario
app.use(express.urlencoded({extended:false}));
//Middleware para que entienda json
app.use(express.json());

app.get('/api/v0/hoteles/:reservaId', auth,(req,res, next) => {
    const coleccion = 'hoteles';
    db.collection(coleccion).findOne({ _id: req.params.reservaId}, (error, elemento) => {
        if (error) return next(error);   //Propagamos el error

        //console.log(elemento);
        res.json({
            resultado: 'ok',
            hoteles: elemento
        });
    });
    
});



app.get('/api/v0/hoteles', auth,(req,res,next) => {
    const coleccion = 'hoteles';
    db.collection(coleccion).find((err, result) =>{
        res.json({
            resultado: 'ok',
            hoteles: result
        });
    });
});


app.post('/api/v0/hoteles',auth, (req, res, next) =>{
    const nuevoElemento = req.body;
    const coleccion = 'hoteles';

    db.collection(coleccion).save(nuevoElemento, (err, elementoGuardado) => {
        if (err) return next(err);

        //console.log(elementoGuardado);
        res.status(201).json({
            resultado: 'ok',
            hoteles: elementoGuardado
        });

    });
});


app.delete('/api/v0/hoteles/:idReserva', auth, (req, res, next) => {
    const idReserva = req.params.idReserva;
    const coleccion = 'hoteles';
    //console.log(typeof idReserva);

    db.collection(coleccion).remove(
        //No vamos a utilizar la función id, porque voy a suponer que lo establezo yo
        //{idReserva: id(idReserva)},
        {_id: idReserva},
        (err, resultado) => {
            if (err) return next(error);

            //console.log(resultado);
            res.json({
                resultado: 'Reserva eliminada correctamente',
                //elementoID: id(idReserva),
                reserva: idReserva,
                resumenOperacion: resultado
            });
        });

});



//Lanzamos el servicio en un modo seguro
https.createServer(HTTPS_OPTIONS, app).listen(port, () => {
    console.log(`Intermediario BD hoteles SEGURO ejecutándose en https://localhost:${port}/`);
});