'use strict'

const urlIntermediarioBD = require('./config').urlIntermediarioBD;
//const urlIntermediarioProveedores = "http://localhost:3201/api/v0";
const urlProveedor = require('./config').urlProveedor;
const {urlPagos} = require('./config')

//Coge la variable de puerto PORT si existe, en caso de que no, pásale el puerto
const {port} = require('./config');
const express = require('express');
const logger = require('morgan');
const app = express();
const axios = require('axios');
const {auth} = require('./services/auth');


//Para que podamos acceder a urls con certificados autofirmados
process.env.NODE_TLS_REJECT_UNAUTHORIZED = 0;

//Declaramos nuestro servicio de https
const https = require('https');

//Cargamos los certificados, necesitamos acceder a los archivos
const fs = require('fs');
const HTTPS_OPTIONS = {
    key: fs.readFileSync('./cert/key.pem'),
    cert: fs.readFileSync('./cert/cert.pem')
};

//Declaramos nuestro middleware
var accessLogStream = fs.createWriteStream('./access.log', {flags:'a'});

app.use(logger('combined', {stream:{
    write: function(str){
        accessLogStream.write(str);
        console.log(str);
    }
}}));
//Middlware para que entienda formato formulario
app.use(express.urlencoded({extended:false}));
//Middleware para que entienda json
app.use(express.json());




app.post('/api/v0/vuelosT/',auth,(req,res, next) => {
    const coleccion = 'vuelos';
    const vuelos = req.body;

    axios.put(`${urlProveedor}/vuelos/${vuelos._id}`, {reservado: 1})
    .then(response => {
        axios.post(`${urlIntermediarioBD}/vuelos`, vuelos,
                            {headers: {"Authorization": `Bearer ${req.usuario.token}`}})
        .then(responseBD => {
            res.json({
                resultado: 'ok',
                vuelos: responseBD.data.vuelos
            })


        })
        .catch(err => {
            //console.log(err)
            axios.put(`${urlProveedor}/vuelos/${vuelos._id}`, {reservado: 0})
            if(err.response){
                res.status(err.response.status).json(err.response.data);
            }
            else{
                res.status(500).json({
                resultado: "Se ha producido un error",
                error: err.message
                })
            }
        })

            })
    .catch(err => {
        //console.log(err)
        if(err.response){
            res.status(err.response.status).json(err.response.data);
        }
        else{
            res.status(500).json({
                resultado: "Se ha producido un error",
                error: err.message
            })
        }
        

    });  
    
    
});

app.post('/api/v0/vuelos/',auth, (req,res, next) => {
    const coleccion = 'vuelos';
    const vuelos = req.body.vuelos;
    var pagos = req.body.pagos;

    axios.put(`${urlProveedor}/vuelos/${vuelos._id}`, {reservado: 1})
    .then(response => {
        axios.post(`${urlIntermediarioBD}/vuelos`, vuelos,
                            {headers: {"Authorization": `Bearer ${req.usuario.token}`}})
        .then(responseBD => {
            pagos.importe = vuelos.precio;
            console.log(pagos);
            axios.post(`${urlPagos}/pagos`, pagos)
                .then(response => {
                    res.json({
                        resultado: 'OK',
                        vuelos: responseBD.data.vuelos
                    });
                })
                .catch(err => {
                    axios.put(`${urlProveedor}/vuelos/${vuelos._id}`, {reservado: 0});
                    axios.delete(`${urlIntermediarioBD}/vuelos/${vuelos._id}`,
                            {headers: {"Authorization": `Bearer ${req.usuario.token}`}})
                    if(err.response){
                        res.status(err.response.status).json(err.response.data);
                    }
                    else{
                        res.status(500).json({
                        resultado: "Se ha producido un error",
                        error: err.message
                        })
                    }


                })

        })
        .catch(err => {
            //console.log(err)
            axios.put(`${urlProveedor}/vuelos/${vuelos._id}`, {reservado: 0})
            if(err.response){
                res.status(err.response.status).json(err.response.data);
            }
            else{
                res.status(500).json({
                resultado: "Se ha producido un error",
                error: err.message
                })
            }
        })

            })
    .catch(err => {
        //console.log(err)
        if(err.response){
            res.status(err.response.status).json(err.response.data);
        }
        else{
            res.status(500).json({
                resultado: "Se ha producido un error",
                error: err.message
            })
        }
        

    });    
});

app.get('/api/v0/vuelos', auth,(req,res,next) => {
    axios.get(`${urlProveedor}/vuelos`)
        .then(response => {
            res.json({
                resultado: "OK",
                vuelos: response.data.vuelos
                
            });
        })
        .catch(function(error){
            console.log("Se ha producido un error");
            console.log(error);
        });

    
});


app.get('/api/v0/vuelosRegistrados', auth,(req,res,next) => {
    axios.get(`${urlIntermediarioBD}/vuelos`,
                    {headers: {"Authorization": `Bearer ${req.usuario.token}`}})
        .then(response => {
            res.json({
                resultado: "OK",
                vuelos: response.data.vuelos
                
            });
        })
        .catch(function(error){
            console.log("Se ha producido un error");
            console.log(error);
        });

    
});




app.delete('/api/v0/vuelosT/:idReserva', auth, (req, res, next) => {
    const idReserva = req.params.idReserva;

    axios.get(`${urlIntermediarioBD}/vuelos/${idReserva}`,
        {headers: {"Authorization": `Bearer ${req.usuario.token}`}})
        .then(response =>{
            //console.log(response)
            if (!response.data.vuelos){
                res.status(400).json({
                    resultado: 'ko',
                    mensaje: 'Reserva no registrada en la base de datos'
                })
            }
            else if(response.data.vuelos.userID != req.usuario._id){
                res.status(400).json({
                    resultado: 'ko',
                    mensaje: 'Estás intentando eliminar una reserva de otro usuario'
                })
            }
            else{
                axios.put(`${urlProveedor}/vuelos/${idReserva}`, {reservado: 0})
                .then(reponseProveedores =>{
                        axios.delete(`${urlIntermediarioBD}/vuelos/${idReserva}`,
                        {headers: {"Authorization": `Bearer ${req.usuario.token}`}})
                            .then(response => {
                                res.json({
                                    resultado: "Reserva eliminada correctamente",
                                    reservaEliminada: response.data.reserva
                                });
                            })
                            .catch(err => {
                                axios.put(`${urlProveedor}/vuelos/${idReserva}`, {reservado: 1});


                                //console.log(err)
                                if(err.response){
                                    res.status(err.response.status).json(err.response.data);
                                }
                                else{
                                    res.status(500).json({
                                        resultado: "Se ha producido un error",
                                        error: err.message
                                    })
                                }
                                

                            })
        
        
                        })

                .catch(err => {
                    //console.log(err)
                    if(err.response){
                        res.status(err.response.status).json(err.response.data);
                    }
                    else{
                        res.status(500).json({
                            resultado: "Se ha producido un error",
                            error: err.message
                        })
                    }
                    
        
                })

            }
        })
        .catch(err => {
            if(err.response){
                res.status(err.response.status).json(err.response.data);
            }
            else{
                res.status(500).json({
                    resultado: "Se ha producido un error",
                    error: err.message
                })
            }
        });  

});

app.delete('/api/v0/vuelos/:idReserva', auth, (req, res, next) => {
    const idReserva = req.params.idReserva;

    axios.get(`${urlIntermediarioBD}/vuelos/${idReserva}`,
        {headers: {"Authorization": `Bearer ${req.usuario.token}`}})
        .then(response =>{
            console.log(response)
            if (!response.data.vuelos){
                res.status(400).json({
                    resultado: 'ko',
                    mensaje: 'Reserva no registrada en la base de datos'
                })
            }
            else if(response.data.vuelos.userID != req.usuario._id){
                res.status(400).json({
                    resultado: 'ko',
                    mensaje: 'Estás intentando eliminar una reserva de otro usuario'
                })
            }
            else{
                var datosPagos = {
                    _id: req.body.pagos._id,
                    importe: -response.data.vuelos.precio
                };


                axios.put(`${urlProveedor}/vuelos/${idReserva}`, {reservado: 0})
                .then(reponseProveedores =>{
                    axios.post(`${urlPagos}/pagos`, datosPagos)
                        .then(responsePagos => {
                            axios.delete(`${urlIntermediarioBD}/vuelos/${idReserva}`,
                                {headers: {"Authorization": `Bearer ${req.usuario.token}`}})
                                .then(response => {
                                    res.json({
                                        resultado: "Reserva eliminada correctamente",
                                        reservaEliminada: response.data.reserva
                                    });
                                })
                                .catch(err => {
                                    datosPagos.importe = -datosPagos.importe;

                                    axios.post(`${urlPagos}/pagos`, datosPagos);
                                    axios.put(`${urlProveedor}/vuelos/${idReserva}`, {reservado: 1});
        
        
                                    //console.log(err)
                                    if(err.response){
                                        res.status(err.response.status).json(err.response.data);
                                    }
                                    else{
                                        res.status(500).json({
                                            resultado: "Se ha producido un error",
                                            error: err.message
                                        })
                                    }
                                    
        
                                })
        
        
                        })
                        .catch(err => {
                            axios.put(`${urlProveedor}/vuelos/${idReserva}`, {reservado: 1});
        
                            if(err.response){
                                res.status(err.response.status).json(err.response.data);
                            }
                            else{
                                res.status(500).json({
                                    resultado: "Se ha producido un error",
                                    error: err.message
                                })
                            }
        
                        })
                })
                .catch(err => {
                    //console.log(err)
                    if(err.response){
                        res.status(err.response.status).json(err.response.data);
                    }
                    else{
                        res.status(500).json({
                            resultado: "Se ha producido un error",
                            error: err.message
                        })
                    }
                    
        
                })

            }
        })
        .catch(err => {
            if(err.response){
                res.status(err.response.status).json(err.response.data);
            }
            else{
                res.status(500).json({
                    resultado: "Se ha producido un error",
                    error: err.message
                })
            }
        });  

});

/*app.listen(port, () => {
    console.log(`API REST gestion_reserva hotel ejecutándose en ${port}`);
});*/

//Lanzamos el servicio en un modo seguro
https.createServer(HTTPS_OPTIONS, app).listen(port, () => {
    console.log(`gestion_reserva_vuelos SEGURO ejecutándose en https://localhost:${port}/`);
});