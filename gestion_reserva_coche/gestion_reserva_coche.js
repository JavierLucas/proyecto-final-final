'use strict'

const urlIntermediarioBD = require('./config').urlIntermediarioBD;
//const urlIntermediarioProveedores = "http://localhost:3200/api/v0";
const urlProveedor = require('./config').urlProveedor;
const {urlPagos} = require('./config');
//Coge la variable de puerto PORT si existe, en caso de que no, pásale el puerto
const {port} = require('./config');
const express = require('express');
const logger = require('morgan');
const app = express();
const axios = require('axios');
const {auth} = require('./services/auth');


//Para que podamos acceder a urls con certificados autofirmados
process.env.NODE_TLS_REJECT_UNAUTHORIZED = 0;


//Declaramos nuestro servicio de https
const https = require('https');

//Cargamos los certificados, necesitamos acceder a los archivos
const fs = require('fs');
const HTTPS_OPTIONS = {
    key: fs.readFileSync('./cert/key.pem'),
    cert: fs.readFileSync('./cert/cert.pem')
};

//Declaramos nuestro middleware

var accessLogStream = fs.createWriteStream('./access.log', {flags:'a'});

app.use(logger('combined', {stream:{
    write: function(str){
        accessLogStream.write(str);
        console.log(str);
    }
}}));
//Middlware para que entienda formato formulario
app.use(express.urlencoded({extended:false}));
//Middleware para que entienda json
app.use(express.json());



app.post('/api/v0/cochesT/',auth,(req,res, next) => {
    const coleccion = 'coches';
    const coches = req.body;

    axios.put(`${urlProveedor}/coches/${coches._id}`, {reservado: 1})
    .then(response => {
        axios.post(`${urlIntermediarioBD}/coches`, coches,
                            {headers: {"Authorization": `Bearer ${req.usuario.token}`}})
        .then(responseBD => {
            res.json({
                resultado: 'ok',
                coches: responseBD.data.coches
            })


        })
        .catch(err => {
            console.log(err)
            axios.put(`${urlProveedor}/coches/${coches._id}`, {reservado: 0})
            if(err.response){
                res.status(err.response.status).json(err.response.data);
            }
            else{
                res.status(500).json({
                resultado: "Se ha producido un error",
                error: err.message
                })
            }
        })

            })
    .catch(err => {
        console.log(err)
        if(err.response){
            res.status(err.response.status).json(err.response.data);
        }
        else{
            res.status(500).json({
                resultado: "Se ha producido un error",
                error: err.message
            })
        }
        

    });  
    
    
});


app.post('/api/v0/coches/',auth, (req,res, next) => {
    const coleccion = 'coches';
    const coches = req.body.coches;
    var pagos = req.body.pagos;

    axios.put(`${urlProveedor}/coches/${coches._id}`, {reservado: 1})
    .then(response => {
        axios.post(`${urlIntermediarioBD}/coches`, coches,
                            {headers: {"Authorization": `Bearer ${req.usuario.token}`}})
        .then(responseBD => {
            pagos.importe = coches.precio;
            //console.log(pagos);
            axios.post(`${urlPagos}/pagos`, pagos)
                .then(response => {
                    res.json({
                        resultado: 'OK',
                        coches: responseBD.data.coches
                    });
                })
                .catch(err => {
                    axios.put(`${urlProveedor}/coches/${coches._id}`, {reservado: 0});
                    axios.delete(`${urlIntermediarioBD}/coches/${coches._id}`,
                            {headers: {"Authorization": `Bearer ${req.usuario.token}`}})
                    if(err.response){
                        res.status(err.response.status).json(err.response.data);
                    }
                    else{
                        res.status(500).json({
                        resultado: "Se ha producido un error",
                        error: err.message
                        })
                    }


                })

        })
        .catch(err => {
            ////console.log(err)
            axios.put(`${urlProveedor}/coches/${coches._id}`, {reservado: 0})
            if(err.response){
                res.status(err.response.status).json(err.response.data);
            }
            else{
                res.status(500).json({
                resultado: "Se ha producido un error",
                error: err.message
                })
            }
        })

            })
    .catch(err => {
        ////console.log(err)
        if(err.response){
            res.status(err.response.status).json(err.response.data);
        }
        else{
            res.status(500).json({
                resultado: "Se ha producido un error",
                error: err.message
            })
        }
        

    });    
});

//Obtenemos las reservas disponibles del proveedor de coches
app.get('/api/v0/coches', auth,(req,res,next) => {
    axios.get(`${urlProveedor}/coches`)
        .then(response => {
            res.json({
                resultado: "OK",
                coches: response.data.coches
                
            });
        })
        .catch(err => {
            ////console.log(err)
            if(err.response){
                res.status(err.response.status).json(err.response.data);
            }
            else{
                res.status(500).json({
                    resultado: "Se ha producido un error",
                    error: err.message
                })
            }
            

        })

    
});


//Obtenemos todas las reservas registradas en nuestra base de datos
app.get('/api/v0/cochesRegistrados', auth,(req,res,next) => {
    axios.get(`${urlIntermediarioBD}/coches`,
                    {headers: {"Authorization": `Bearer ${req.usuario.token}`}})
        .then(response => {
            res.json({
                resultado: "OK",
                coches: response.data.coches
                
            });
        })
        .catch(err => {
            ////console.log(err)
            if(err.response){
                res.status(err.response.status).json(err.response.data);
            }
            else{
                res.status(500).json({
                    resultado: "Se ha producido un error",
                    error: err.message
                })
            }
            

        })

    
});


app.delete('/api/v0/cochesT/:idReserva', auth, (req, res, next) => {
    const idReserva = req.params.idReserva;

    axios.get(`${urlIntermediarioBD}/coches/${idReserva}`,
        {headers: {"Authorization": `Bearer ${req.usuario.token}`}})
        .then(response =>{
            //console.log(response)
            if (!response.data.coches){
                res.status(400).json({
                    resultado: 'ko',
                    mensaje: 'Reserva no registrada en la base de datos'
                })
            }
            else if(response.data.coches.userID != req.usuario._id){
                res.status(400).json({
                    resultado: 'ko',
                    mensaje: 'Estás intentando eliminar una reserva de otro usuario'
                })
            }
            else{
                axios.put(`${urlProveedor}/coches/${idReserva}`, {reservado: 0})
                .then(reponseProveedores =>{
                        axios.delete(`${urlIntermediarioBD}/coches/${idReserva}`,
                        {headers: {"Authorization": `Bearer ${req.usuario.token}`}})
                            .then(response => {
                                res.json({
                                    resultado: "Reserva eliminada correctamente",
                                    reservaEliminada: response.data.reserva
                                });
                            })
                            .catch(err => {
                                axios.put(`${urlProveedor}/coches/${idReserva}`, {reservado: 1});


                                //console.log(err)
                                if(err.response){
                                    res.status(err.response.status).json(err.response.data);
                                }
                                else{
                                    res.status(500).json({
                                        resultado: "Se ha producido un error",
                                        error: err.message
                                    })
                                }
                                

                            })
        
        
                        })

                .catch(err => {
                    //console.log(err)
                    if(err.response){
                        res.status(err.response.status).json(err.response.data);
                    }
                    else{
                        res.status(500).json({
                            resultado: "Se ha producido un error",
                            error: err.message
                        })
                    }
                    
        
                })

            }
        })
        .catch(err => {
            if(err.response){
                res.status(err.response.status).json(err.response.data);
            }
            else{
                res.status(500).json({
                    resultado: "Se ha producido un error",
                    error: err.message
                })
            }
        });  

});

app.delete('/api/v0/coches/:idReserva', auth, (req, res, next) => {
    const idReserva = req.params.idReserva;

    axios.get(`${urlIntermediarioBD}/coches/${idReserva}`,
        {headers: {"Authorization": `Bearer ${req.usuario.token}`}})
        .then(response =>{
            console.log(response)
            if (!response.data.coches){
                res.status(400).json({
                    resultado: 'ko',
                    mensaje: 'Reserva no registrada en la base de datos'
                })
            }
            else if(response.data.coches.userID != req.usuario._id){
                res.status(400).json({
                    resultado: 'ko',
                    mensaje: 'Estás intentando eliminar una reserva de otro usuario'
                })
            }
            else{
                var datosPagos = {
                    _id: req.body.pagos._id,
                    importe: -response.data.coches.precio
                };


                axios.put(`${urlProveedor}/coches/${idReserva}`, {reservado: 0})
                .then(reponseProveedores =>{
                    axios.post(`${urlPagos}/pagos`, datosPagos)
                        .then(responsePagos => {
                            axios.delete(`${urlIntermediarioBD}/coches/${idReserva}`,
                                {headers: {"Authorization": `Bearer ${req.usuario.token}`}})
                                .then(response => {
                                    res.json({
                                        resultado: "Reserva eliminada correctamente",
                                        reservaEliminada: response.data.reserva
                                    });
                                })
                                .catch(err => {
                                    datosPagos.importe = -datosPagos.importe;

                                    axios.post(`${urlPagos}/pagos`, datosPagos);
                                    axios.put(`${urlProveedor}/coches/${idReserva}`, {reservado: 1});
        
        
                                    //console.log(err)
                                    if(err.response){
                                        res.status(err.response.status).json(err.response.data);
                                    }
                                    else{
                                        res.status(500).json({
                                            resultado: "Se ha producido un error",
                                            error: err.message
                                        })
                                    }
                                    
        
                                })
        
        
                        })
                        .catch(err => {
                            axios.put(`${urlProveedor}/coches/${idReserva}`, {reservado: 1});
        
                            if(err.response){
                                res.status(err.response.status).json(err.response.data);
                            }
                            else{
                                res.status(500).json({
                                    resultado: "Se ha producido un error",
                                    error: err.message
                                })
                            }
        
                        })
                })
                .catch(err => {
                    //console.log(err)
                    if(err.response){
                        res.status(err.response.status).json(err.response.data);
                    }
                    else{
                        res.status(500).json({
                            resultado: "Se ha producido un error",
                            error: err.message
                        })
                    }
                    
        
                })

            }
        })
        .catch(err => {
            if(err.response){
                res.status(err.response.status).json(err.response.data);
            }
            else{
                res.status(500).json({
                    resultado: "Se ha producido un error",
                    error: err.message
                })
            }
        });  

});




//Lanzamos el servicio en un modo seguro
https.createServer(HTTPS_OPTIONS, app).listen(port, () => {
    console.log(`gestion_reserva_coches SEGURO ejecutándose en https://localhost:${port}/`);
});