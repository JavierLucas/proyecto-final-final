'use strict'

const ROL = {
    ADMIN: 'admin',
    BASIC: 'basic'
}

module.exports = {
    ROL: ROL
}