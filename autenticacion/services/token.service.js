'use strict'

const jwt = require('jwt-simple');
const moment = require('moment')

const SECRET = require('../config').secret;
const EXP_TIME = require('../config').tokenExptime;

//Crear token
//
// Devolver un token tipo JWT
// Formato JWT:
//  HEADER.PAYLOAD.VERIFY_SIGNATURE
// Donde:
//      HEADER (objeto JSON con algoritmo y codificado en formato base64URL)
//      Traer ejemplo de la web
//      PAYLOAD
//      VERIFIED_SIGNATURE = (base64URLEncoded(HEADER) + "." +
//      base64URLEncoded(PAYLOAD), SECRET)

function creaToken(user){
    const payload = {
        sub: user._id,
        rol: user.rol,
        iat: moment().unix(),
        exp: moment().add(EXP_TIME, 'minutes').unix()
    };
    return jwt.encode(payload, SECRET);
};

//decodificaToken 
// Dado un token jwt, nos dice si es correcto o si está caducado

function decodificaToken(token){
    //Creamos una promesa
    return new Promise((resolve, reject) => {
        try{
            //El true hace que no sale error si el token está caducado
            const payload = jwt.decode(token, SECRET, false);

            resolve(payload.sub);
        }
        catch (err){
            reject({
                status: 500,
                message: 'El token no es válido'
            });

        }
    });
}

module.exports = {
    creaToken,
    decodificaToken
};