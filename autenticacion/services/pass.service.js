'use strict'

const bcrypt = require('bcrypt');

//Función que devuelve un hash con un salt incluido en el formato:
////Formato de un hash con salt:
//  $2b$16$OGZdFg6xS2CZcqYKjlk50.IDU.5fdxAqIg1ZKuKBAyuzAX4eiRCnq
//    ----***---------------------********************************
//  alg    cost    salt             hash

function encriptaPassword(password){
    return bcrypt.hashSync(password, 10);
}

//Función que comparar el password  con el hash almacenado
function comparaPassword(password, hash){
    return bcrypt.compareSync(password, hash);
}

//Para poder exportar las funciones
module.exports = {
    encriptaPassword,
    comparaPassword
};