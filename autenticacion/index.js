'use-strict'

const {port} = require('./config');
const express = require('express');
const logger = require('morgan');
const mongojs = require('mongojs');
const passService = require('./services/pass.service');
const tokenService = require('./services/token.service');
const ROL = require('./roles').ROL;
const dbURL = require('./config').dbURL;


const app = express();


//var db = mongojs('localhost:27017/usuarios');
var db = mongojs(dbURL);
//Declaramos nuestro servicio de https
const https = require('https');

//Cargamos los certificados, necesitamos acceder a los archivos
const fs = require('fs');
const HTTPS_OPTIONS = {
    key: fs.readFileSync('./cert/key.pem'),
    cert: fs.readFileSync('./cert/cert.pem')
};

//Declaramos nuestro middleware
var accessLogStream = fs.createWriteStream('./access.log', {flags:'a'});

app.use(logger('combined', {stream:{
    write: function(str){
        accessLogStream.write(str);
        console.log(str);
    }
}}));
//Middlware para que entienda formato formulario
app.use(express.urlencoded({extended:false}));
//Middleware para que entienda json
app.use(express.json());


app.post('/api/v0/registro', (req, res, next) => {
    var nuevoUsuario = req.body;
    nuevoUsuario.rol = ROL.BASIC;
    const coleccion = "usuarios";

    db.collection(coleccion).findOne({_id: req.body._id}, (err, doc) => {
        if (doc){
            res.status(400).json({
                resultado: 'ko',
                mensaje: `Ya exite un usuario registrado con el correo ${req.body._id}`
            })
        }


        else{
            nuevoUsuario.password = passService.encriptaPassword(nuevoUsuario.password)

            db.collection(coleccion).save(nuevoUsuario, (err, elementoGuardado) => {
                if (err) return res.status(500).json({
                    resultado: "Se ha producido un error en el servidor"
                })
                console.log(elementoGuardado);
                const token = tokenService.creaToken(nuevoUsuario);
                res.status(201).json({
                    resultado: "ok",
                    token: token
                });
            })
        }
    })


});

app.get('/api/v0/usuarios', (req,res,next) => {
    const coleccion = 'usuarios';
    db.collection(coleccion).find((err, result) =>{
        res.json({
            resultado: 'ok',
            usuarios: result
        });
    });
});

    

//Conectamos con la base de datos y vemos si el usuario que se nos pasa es correcto
app.post('/api/v0/login', (req, res, err) => {
    var datos = req.body;
    const _id = datos._id;
    const password = datos.password;
    const coleccion = 'usuarios'

    console.log(datos)

    db.collection(coleccion).findOne({_id: _id}, (error, elemento) => {
        if (error) return res.status(500).json({
            resultado: "Se ha producido un error en el servidor"
        });
        if (!elemento){
            res.status(400).json({
                resultado: "Usuario no encontrado"
            });
        }
        else if (passService.comparaPassword(password, elemento.password)){
            res.json({
                resultado: 'OK',
                token: tokenService.creaToken(elemento)
            });
        }
        else{
            res.status(401).json({resultado: 'Contraseña incorrecta'});
        }
       
    });

});

//Lanzamos el servicio en un modo seguro
https.createServer(HTTPS_OPTIONS, app).listen(port, () => {
    console.log(`autenticación SEGURO ejecutándose en https://localhost:${port}/`);
});