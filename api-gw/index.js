'use-strict'

const express = require('express');
const logger = require('morgan');
const config = require('./config');
const {ROL} = require('./roles');
const port = config.port;

const app = express();
const axios = require('axios');
const helmet = require('helmet');

//Para que podamos acceder a urls con certificados autofirmados
process.env.NODE_TLS_REJECT_UNAUTHORIZED = 0;

//Declaramos nuestro servicio de https
const https = require('https');



//Cargamos los certificados, necesitamos acceder a los archivos
const fs = require('fs');
const HTTPS_OPTIONS = {
    key: fs.readFileSync('./cert/key.pem'),
    cert: fs.readFileSync('./cert/cert.pem')
};

//Declaramos nuestro middleware

var accessLogStream = fs.createWriteStream('./access.log', {flags:'a'});

app.use(logger('combined', {stream:{
    write: function(str){
        accessLogStream.write(str);
        console.log(str);
    }
}}));

app.use(helmet());
//Middlware para que entienda formato formulario
app.use(express.urlencoded({extended:false}));
//Middleware para que entienda json
app.use(express.json());

const {auth, authRol} = require('./services/auth');
const { query, response } = require('express');
const { default: contentSecurityPolicy } = require('helmet/dist/middlewares/content-security-policy');





//VUELOS
app.post('/api/v0/vuelos/',auth,(req,res, next) => {
    const datos = req.body;
    datos.vuelos.userID = req.usuario._id;

    axios.post(`${config.servicioVuelos}/vuelos`, datos,
                    {headers: {"Authorization": `Bearer ${req.usuario.token}`}})
        .then(response => {
            res.json({
                resultado: 'OK',
                vuelos: response.data.vuelos
            });
        })
        .catch(err => {
            //console.log(err)
            if(err.response){
                res.status(err.response.status).json(err.response.data);
            }
            else{
                res.status(500).json({
                    resultado: "Se ha producido un error",
                    error: err.message
                })
            }
            

        })

    
});

app.get('/api/v0/vuelos', auth,(req,res,next) => {
    //console.log(req.query.precioMax);
    axios.get(`${config.servicioVuelos}/vuelos`,
                    {headers: {"Authorization": `Bearer ${req.usuario.token}`}})
        .then(response => {            
            const vuelosFiltrados = response.data.vuelos.filter(vuelo => {
                //console.log(`Precio Max = ${req.query.precioMax}, Precio vuelo: ${vuelo.precio}`);
                //console.log((req.query.precioMax ? (vuelo.precio <= req.query.precioMax) : true))
                return ((req.query.fechaInicio ? (vuelo.fecha >= req.query.fechaInicio) : true) &&
                        (req.query.fechaFin ? (vuelo.fecha <= req.query.fechaFin) : true) &&
                        (req.query.paisOrigen ? (req.query.paisOrigen === vuelo.paisOrigen) : true) &&
                        (req.query.ciudadOrigen ? (req.query.ciudadOrigen === vuelo.ciudadOrigen) : true) &&
                        (req.query.paisDestino ? (req.query.paisDestino === vuelo.paisDestino) : true) &&
                        (req.query.ciudadDestino ? (req.query.ciudadDestino === vuelo.ciudadDestino) : true) &&
                        (req.query.precioMax ? (vuelo.precio <= req.query.precioMax) : true));
                        
            });
            res.json({
                resultado: "OK",
                vuelos: vuelosFiltrados
                
            });
        })
        .catch(err => {
            ////console.log(err)
            if(err.response){
                res.status(err.response.status).json(err.response.data);
            }
            else{
                res.status(500).json({
                    resultado: "Se ha producido un error",
                    error: err.message
                })
            }
            

        })

    
});



app.get('/api/v0/vuelosRegistrados', auth, authRol(ROL.ADMIN),(req,res,next) => {
    ////console.log(req.query.precioMax);
    axios.get(`${config.servicioVuelos}/vuelosRegistrados`,
                    {headers: {"Authorization": `Bearer ${req.usuario.token}`}})
        .then(response => {    
            res.json({
                resultado: "OK",
                vuelos: response.data.vuelos
                
            });
        })
        .catch(err => {
            ////console.log(err)
            if(err.response){
                res.status(err.response.status).json(err.response.data);
            }
            else{
                res.status(500).json({
                    resultado: "Se ha producido un error",
                    error: err.message
                })
            }
            

        })

    
});


app.get('/api/v0/misVuelos', auth,(req,res,next) => {
    ////console.log(req.query.precioMax);
    axios.get(`${config.servicioVuelos}/vuelosRegistrados`,
                    {headers: {"Authorization": `Bearer ${req.usuario.token}`}})
        .then(response => {    
            const vuelosFiltrados = response.data.vuelos.filter(vuelo => vuelo.userID === req.usuario._id );
            res.json({
                resultado: "OK",
                vuelos: vuelosFiltrados
                
            });
        })
        .catch(err => {
            ////console.log(err)
            if(err.response){
                res.status(err.response.status).json(err.response.data);
            }
            else{
                res.status(500).json({
                    resultado: "Se ha producido un error",
                    error: err.message
                })
            }
            

        })

    
});

app.delete('/api/v0/vuelos/:idReserva', auth, (req, res, next) => {
    const idReserva = req.params.idReserva;
    const data = req.body;
    axios.delete(`${config.servicioVuelos}/vuelos/${idReserva}`,
                    {headers: {"Authorization": `Bearer ${req.usuario.token}`},
                    data: data})
        .then(response => {
            res.json({
                resultado: "Reserva eliminada correctamente",
                reservaEliminada: response.data.reservaEliminada
            });
        })
        .catch(err => {
            ////console.log(err)
            if(err.response){
                res.status(err.response.status).json(err.response.data);
            }
            else{
                res.status(500).json({
                    resultado: "Se ha producido un error",
                    error: err.message
                })
            }
            

        })

    

});





//Coches
app.post('/api/v0/coches/',auth,(req,res, next) => {
    const datos = req.body;
    datos.coches.userID = req.usuario._id;
    //console.log(datos);

    //console.log(`_id: ${datos._id}`)

    

    axios.post(`${config.servicioCoches}/coches`, datos,
                    {headers: {"Authorization": `Bearer ${req.usuario.token}`}})
        .then(response => {
            res.json({
                resultado: 'OK',
                coches: response.data.coches
            });
        })
        .catch(err => {
            ////console.log(err)
            if(err.response){
                res.status(err.response.status).json(err.response.data);
            }
            else{
                res.status(500).json({
                    resultado: "Se ha producido un error",
                    error: err.message
                })
            }
            

        })

    
});


app.get('/api/v0/coches', auth,(req,res,next) => {

    axios.get(`${config.servicioCoches}/coches`,
                    {headers: {"Authorization": `Bearer ${req.usuario.token}`}})    
        .then(response => {
            const cochesFiltrados = response.data.coches.filter(c => {
                return (req.query.fechaInicio ? (c.fecha >= req.query.fechaInicio) : true) &&
                    (req.query.fechaFin ? (c.fecha <= req.query.fechaFin) : true) &&
                    (req.query.pais ? (c.pais === req.query.pais) : true) &&
                    (req.query.ciudad ? (c.ciudad === req.query.ciudad) : true) &&
                    (req.query.precioMax ? (c.precio <= req.query.precioMax) : true) &&
                    (req.query.marca ? (c.coche.marca === req.query.marca) : true);
            });

            res.json({
                resultado: "OK",
                coches: cochesFiltrados
                
            });
        })
        .catch(err => {
            ////console.log(err)
            if(err.response){
                res.status(err.response.status).json(err.response.data);
            }
            else{
                res.status(500).json({
                    resultado: "Se ha producido un error",
                    error: err.message
                })
            }
            

        })

    
});


app.delete('/api/v0/coches/:idReserva', auth, (req, res, next) => {
    const idReserva = req.params.idReserva;
    const data = req.body;
    axios.delete(`${config.servicioCoches}/coches/${idReserva}`,
                        {headers: {"Authorization": `Bearer ${req.usuario.token}`},
                            data: data})
        .then(response => {
            res.json({
                resultado: "Reserva eliminada correctamente",
                reservaEliminada: response.data.reservaEliminada
            });
        })
        .catch(err => {
            ////console.log(err)
            if(err.response){
                res.status(err.response.status).json(err.response.data);
            }
            else{
                res.status(500).json({
                    resultado: "Se ha producido un error",
                    error: err.message
                })
            }
            

        })

    

});

app.get('/api/v0/cochesRegistrados', auth,authRol(ROL.ADMIN),(req,res,next) => {

    axios.get(`${config.servicioCoches}/cochesRegistrados`,
                    {headers: {"Authorization": `Bearer ${req.usuario.token}`}})    
        .then(response => {
            res.json({
                resultado: "OK",
                coches: response.data.coches
                
            });
        })
        .catch(err => {
            ////console.log(err)
            if(err.response){
                res.status(err.response.status).json(err.response.data);
            }
            else{
                res.status(500).json({
                    resultado: "Se ha producido un error",
                    error: err.message
                })
            }
            

        })

    
});


app.get('/api/v0/misCoches', auth,(req,res,next) => {
    ////console.log(req.query.precioMax);
    axios.get(`${config.servicioCoches}/cochesRegistrados`,
                    {headers: {"Authorization": `Bearer ${req.usuario.token}`}})
        .then(response => {    
            const cochesFiltrados = response.data.coches.filter(coche => coche.userID === req.usuario._id );
            res.json({
                resultado: "OK",
                coches: cochesFiltrados
                
            });
        })
        .catch(err => {
            ////console.log(err)
            if(err.response){
                res.status(err.response.status).json(err.response.data);
            }
            else{
                res.status(500).json({
                    resultado: "Se ha producido un error",
                    error: err.message
                })
            }
            

        })

    
});


//HOTELES
app.post('/api/v0/hoteles/',auth,(req,res, next) => {
    const datos = req.body;
    datos.hoteles.userID = req.usuario._id;
    ////console.log(datos);

    axios.post(`${config.servicioHoteles}/hoteles`, datos,
                        {headers: {"Authorization": `Bearer ${req.usuario.token}`}})
        .then(response => {
            res.json({
                resultado: 'OK',
                hoteles: response.data.hoteles
            });
        })
        .catch(err => {
            ////console.log(err)
            if(err.response){
                res.status(err.response.status).json(err.response.data);
            }
            else{
                res.status(500).json({
                    resultado: "Se ha producido un error",
                    error: err.message
                })
            }
            

        })

    
});


app.get('/api/v0/hoteles', auth, (req,res,next) => {
    ////console.log(req.params.usuario);
    axios.get(`${config.servicioHoteles}/hoteles`, 
                {headers: {"Authorization": `Bearer ${req.usuario.token}`}})
        .then(response => {
            //console.log(req.query)
            const hotelesFiltrados = response.data.hoteles.filter(hotel => {
                return ((req.query.fechaInicio ? (hotel.fecha >= req.query.fechaInicio) : true) &&
                        (req.query.fechaFin ? (hotel.fecha <= req.query.fechaFin) : true) &&
                        (req.query.pais ? (hotel.pais === req.query.pais) : true) &&
                        (req.query.ciudad ? (hotel.ciudad === req.query.ciudad) : true) &&
                        (req.query.estrellasMinimas ? (hotel.estrellas >= req.query.estrellasMinimas) : true) &&
                        (req.query.precioMax ? (hotel.precio <= req.query.precioMax) : true));

            });
            res.json({
                resultado: "OK",
                hoteles: hotelesFiltrados
                
            });
        })
        .catch(err => {
            ////console.log(err)
            if(err.response){
                res.status(err.response.status).json(err.response.data);
            }
            else{
                res.status(500).json({
                    resultado: "Se ha producido un error",
                    error: err.message
                })
            }
            

        })

    
});


app.delete('/api/v0/hoteles/:idReserva', auth, (req, res, next) => {
    const idReserva = req.params.idReserva;
    const data = req.body;
    axios.delete(`${config.servicioHoteles}/hoteles/${idReserva}`,
                    {headers: {"Authorization": `Bearer ${req.usuario.token}`},
                        data: data})
        .then(response => {
            res.json({
                resultado: "Reserva eliminada correctamente",
                reservaEliminada: response.data.reservaEliminada
            });
        })
        .catch(err => {
            ////console.log(err)
            if(err.response){
                res.status(err.response.status).json(err.response.data);
            }
            else{
                res.status(500).json({
                    resultado: "Se ha producido un error",
                    error: err.message
                })
            }
            

        })

    

});


app.get('/api/v0/hotelesRegistrados', auth,authRol(ROL.ADMIN),(req,res,next) => {
    ////console.log(req.params.usuario);
    axios.get(`${config.servicioHoteles}/hotelesRegistrados`,
                    {headers: {"Authorization": `Bearer ${req.usuario.token}`}})
        .then(response => {
            res.json({
                resultado: "OK",
                hoteles: response.data.hoteles
                
            });
        })
        .catch(err => {
            ////console.log(err)
            if(err.response){
                res.status(err.response.status).json(err.response.data);
            }
            else{
                res.status(500).json({
                    resultado: "Se ha producido un error",
                    error: err.message
                })
            }
            

        })

    
});


app.get('/api/v0/misHoteles', auth,(req,res,next) => {
    ////console.log(req.query.precioMax);
    axios.get(`${config.servicioHoteles}/hotelesRegistrados`,
                    {headers: {"Authorization": `Bearer ${req.usuario.token}`}})
        .then(response => {    
            const hotelesFiltrados = response.data.hoteles.filter(hotel => hotel.userID === req.usuario._id );
            res.json({
                resultado: "OK",
                hoteles: hotelesFiltrados
                
            });
        })
        .catch(err => {
            ////console.log(err)
            if(err.response){
                res.status(err.response.status).json(err.response.data);
            }
            else{
                res.status(500).json({
                    resultado: "Se ha producido un error",
                    error: err.message
                })
            }
            

        })
});


app.get('/api/v0/misReservas', auth,(req,res, err) =>{
    var resultado = {};
    const vuelos = axios.get(`https://localhost:23176/api/v0/vuelos`, {
        headers: {Authorization: `Bearer ${req.usuario.token}`}
    });
    const hoteles = axios.get(`https://localhost:23175/api/v0/hoteles`, {
        headers: {Authorization: `Bearer ${req.usuario.token}`}
    });
    const coches = axios.get(`https://localhost:23177/api/v0/coches`, {
        headers: {Authorization: `Bearer ${req.usuario.token}`}
    });

    axios.all([vuelos, hoteles, coches])
        .then(axios.spread((...responses) => {
            resultado.vuelos = responses[0].data.vuelos.filter(vuelo => vuelo.userID === req.usuario._id);
            resultado.hoteles = responses[1].data.hoteles.filter(hotel => hotel.userID === req.usuario._id);
            resultado.coches = responses[2].data.coches.filter(coche => coche.userID === req.usuario._id);

            res.json(resultado);
            
    
        }))
        .catch(err => {
            ////console.log(err)
            if(err.response){
                res.status(err.response.status).json(err.response.data);
            }
            else{
                res.status(500).json({
                    resultado: "Se ha producido un error",
                    error: err.message
                })
            }
            

        })
})


//PAQUETES

app.post('/api/v0/paquetes', auth, (req,res,next) =>{
    const data = req.body;
    axios.post(`${config.servicioPaquetes}/paquetes`,data, {
        headers: {Authorization: `Bearer ${req.usuario.token}`}
    })
        .then(response =>{
            res.json(response.data);
        })
        .catch(err => {
            ////console.log(err)
            if(err.response){
                res.status(err.response.status).json(err.response.data);
            }
            else{
                res.status(500).json({
                    resultado: "Se ha producido un error",
                    error: err.message
                })
            }
            

        })
    
})

//AUTENTICACION Y AUTORIZACION
app.post('/api/v0/registro', (req, res, next) => {
    var nuevoUsuario = req.body;

    axios.post(`${config.servicioAutenticacion}/registro`, nuevoUsuario)
        .then(response => {
            res.json(response.data);
        })
        .catch(err => {
            ////console.log(err)
            if(err.response){
                res.status(err.response.status).json(err.response.data);
            }
            else{
                res.status(500).json({
                    resultado: "Se ha producido un error",
                    error: err.message
                })
            }
            

        })
});


app.post('/api/v0/login', (req, res, next) => {
    var datos = req.body;
    const _id = datos._id;
    const password = datos.password;
    const coleccion = 'usuarios'
    //console.log(datos);

    axios.post(`${config.servicioAutenticacion}/login`, datos)
        .then(response => {
            //console.log(response.data);
            res.json(response.data)
        })
        .catch(err => {
            ////console.log(err)
            if(err.response){
                res.status(err.response.status).json(err.response.data);
            }
            else{
                res.status(500).json({
                    resultado: "Se ha producido un error",
                    error: err.message
                })
            }
            

        })
            
});

app.get('/api/v0/usuarios', auth,authRol(ROL.ADMIN),(req,res,next) => {
    axios.get(`${config.servicioAutenticacion}/usuarios`)
        .then((result) => {
            res.json(result.data)            
        })
        .catch(err => {
            ////console.log(err)
            if(err.response){
                res.status(err.response.status).json(err.response.data);
            }
            else{
                res.status(500).json({
                    resultado: "Se ha producido un error",
                    error: err.message
                })
            }
            

        })


})


https.createServer(HTTPS_OPTIONS, app).listen(port, () => {
    console.log(`API GW SEGURO ejecutándose en https://localhost:${port}/`);
});