module.exports = {
    servicioCoches: 'https://localhost:23077/api/v0',
    servicioHoteles: 'https://localhost:23075/api/v0',
    servicioVuelos:'https://localhost:23076/api/v0',
    servicioPaquetes: 'https://localhost:23078/api/v0',
    servicioAutenticacion:'https://localhost:23575/api/v0',
    port: 23675,
    secret: 'secreto1234',
    tokenExptime: 24*60
};