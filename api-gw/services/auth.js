'use-strict'


const tokenService = require('./token.service')

// Creamos un Middleware de Autorización tipo Bearer
function auth(req, res, next){

    if(!req.headers.authorization){
        res.status(401).json({
            resultado: 'KO',
            mensaje: 'No se ha enviado el token tipo Bearer en la cabecera Authorization'
        });
        return next(new Error("Falta el token"));
    }

    const token = req.headers.authorization.split(" ")[1];

    tokenService.decodificaToken(token)
        .then(result => {
            console.log(result)
            //req.params.token = token;
            //req.params.usuario = result.sub;
            //req.params.rol = result.rol;
            req.usuario = {
                token: token,
                _id: result.sub,
                rol: result.rol
            }
            next();
        })
        .catch(err => {
            res.status(err.status).json({
                resultado: 'KO',
                mensaje: err.mensaje
            });
            //console.log(err);
            next(new Error("Error"))
        })

};

function authRol(rol){
    return (req, res, next) => {
        if (req.usuario.rol !== rol){
            res.status(403).json({
                resultado: 'KO',
                mensaje: "No tienes autorizacion para acceder a este recurso"
            });
            return
        };
        next();

    };
}


module.exports = {
    auth,
    authRol
}