'use strict'

const jwt = require('jwt-simple');
const moment = require('moment')

const SECRET = require('../config').secret;
const EXP_TIME = require('../config').tokenExptime;

//decodificaToken 
// Dado un token jwt, nos dice si es correcto o si está caducado

function decodificaToken(token){
    //Creamos una promesa
    return new Promise((resolve, reject) => {
        try{
            //El true hace que no sale error si el token está caducado
            const payload = jwt.decode(token, SECRET, false);

 
            resolve(payload);
        }
        catch (err){
            reject({
                status: 403,
                mensaje: 'El token no es válido'
            });

        }
    });
}

module.exports = {
    decodificaToken
};